import os
import re
from datetime import datetime
from itertools import chain
from subprocess import run
from sys import argv
from textwrap import indent

import requests

from utils import ROOT_FOLDER, get_po_folder, get_project_paths, load_projects


STATE = {
    "IS_LANGUAGE_TOOL_RUNNING": False,
}


def check_spelling(project, paths):
    base_command = [
        "posieve",
        "--skip-obsolete",
        "check-spell-ec",
        "-s", "lokalize",
        "-s", "allow_msgid_words",
        "-s", "uncamel_bad",
    ]
    optional_params = []
    if "accel" in project:
        optional_params.extend(["-s", f"accel:{project['accel']}"])
    if "allow_word_patterns" in project:
        pattern = "|".join(project["allow_word_patterns"])
        optional_params.extend(["-s", f"skip:{pattern}"])
    result = run(
        [*base_command, *optional_params, *paths],
        cwd=get_po_folder(project),
        capture_output=True,
    )
    if b"Encountered" in result.stdout:
        print(f"{project['id']}:")
        print(indent(result.stdout.decode(), " "*2))
        passed = False
    else:
        passed = result.returncode == 0
        if not passed:
            print(result.stderr.decode())
    return passed


def check_grammar(project, paths):
    if not STATE["IS_LANGUAGE_TOOL_RUNNING"]:
        while True:
            try:
                response = requests.get("http://localhost:8081/v2/languages")
            except requests.exceptions.ConnectionError:
                pass
            else:
                if response.status_code == 200:
                    break
        STATE["IS_LANGUAGE_TOOL_RUNNING"] = True

    disabled_languagetool_rules = [
        # Predeterminadas de Pology.
        "COMMA_PARENTHESIS_WHITESPACE",
        "HUNSPELL_RULE",
        "UPPERCASE_SENTENCE_START",

        # Falsos positivos por erros en LanguageTool.
        "DASH_ENUMERATION_SPACE_RULE",
        "DASH_RULE",
        "GENERAL_GENDER_AGREEMENT_ERRORS",
        "GENERAL_NUMBER_AGREEMENT_ERRORS",
        "GENERAL_NUMBER_FORMAT",
        "GENERAL_VERB_AGREEMENT_ERRORS",
        "GL_WORDINESS_REPLACE",
        "LOOSE_ACCENTS",
        "PARENTESESE_AND_QUOTES_SPACING",
        "PLURAL_IN_UNITS_OF_MEASURE",
        "RECORD",
        "REFLEXIVOS",
        "SEMICOLON_COLON_SPACING",
        "SENTENCE_WHITESPACE",
        "SPACE_BEFORE_PUNCTUATION",
        "TIBURON",
        "TIME_FORMAT",
        "UNPAIRED_BRACKETS",
        "WHITESPACE_RULE",
        "WORD_REPETITION",

        # Falsos positivos por erros en Pology.
        "ARROWS",  # HTML
        "CHEMICAL_FORMULAS_TYPOGRAPHY",  # HTML
        "GL_BARBARISM_REPLACE",  # HTML
        "UNITS_OF_MEASURE_SPACING",  # Extensións de ficheiro

        # Falsos positivos que habería que silenciar só para as mensaxes
        # afectadas.
        "CALAMAR",
        "DOUBLE_PUNCTUATION",
        "EQUAL_OR_X",
        "GL_CAST_WORDS",
        "GL_REDUNDANCY_REPLACE",
        "GL_WIKIPEDIA_COMMON_ERRORS",
        "NO_CACOPHONY",
        "PHRASE_REPETITION",
        "PLUS_MINUS",
        "R_SYMBOL",
        "RACHA",
        "SEMICOLON_AND_QUOTES",
        "SPACE_AFTER_PUNCTUATION",
        "SPACES_BETWEEN_OPERATORS",
    ]

    base_command = [
        "posieve",
        "--skip-obsolete",
        "check-grammar",
        "-s", "lokalize",
        "-s", "lang:gl",
        "-s", "filter:remove/remove_markup_text",
        *chain(
            *(
                ("-s", f"disable:{rule}")
                for rule in disabled_languagetool_rules
            ),
        ),
    ]

    result = run(
        [*base_command, *paths],
        cwd=get_po_folder(project),
        capture_output=True,
    )
    if result.stdout:
        print(f"{project['id']}:")
        print(indent(result.stdout.decode(), " "*2))
        passed = False
    else:
        passed = result.returncode == 0
        if not passed:
            print(result.stderr.decode())
    return passed


def check_rules(project, paths):
    rule_prefixes = tuple()
    rules = tuple()
    skip_rules = (
        "gnome-gl_predefinido",  # «Predefinido» pode acaer para «predefined» e «preset».
        "preacordo-daemon-daemon",  # → proposta-daemon-servizo
        "preacordo-print_preview",  # → proposta-print-preview
        "proposta-app-apli",  # → proposta-app-aplicación
        "proposta-válido-correcto",  # «Válido» pode acaer.
        "trasno-applet",  # Parece haber consenso en volver a «miniaplicación».
        "trasno-chat",  # → proposta-chat
        "trasno-chat_reverse",  # → proposta-chat
    )
    envs = [
        project['id'],
        *project.get('envs', []),
    ]
    base_command = [
        "posieve",
        "--skip-obsolete",
        "check-rules",
        "-s",
        "lokalize",
        "-s",
        f"env:{','.join(envs)}",
    ]
    rule_args = []
    if rule_prefixes:
        pattern = f"^({'|'.join(re.escape(prefix) for prefix in rule_prefixes)})"
        rule_args.extend(
            (
                "-s",
                f"rulerx:{pattern}",
            )
        )
    if rules:
        rule_args.extend(
            (
                "-s",
                f"rule:{','.join(rules)}",
            )
        )
    if skip_rules:
        rule_args.extend(
            (
                "-s",
                f"norule:{','.join(skip_rules)}",
            )
        )
    result = run(
        [*base_command, *rule_args, *paths],
        cwd=get_po_folder(project),
        capture_output=True,
    )
    if result.stdout:
        print(f"{project['id']}:")
        print(indent(result.stdout.decode(), " "*2))
        passed = False
    else:
        passed = result.returncode == 0
        if not passed:
            print(result.stderr.decode())
    return passed


def _get_project_paths(projects, newer_than):
    project_paths = []
    for project in projects:
        folder = get_po_folder(project)
        paths = sorted(
            path
            for path in get_project_paths(project)
            if newer_than is None or os.path.getctime(os.path.join(folder, path)) > newer_than
        )
        if not paths:
            continue
        project_paths.append([project, paths])
    return project_paths


def main():
    projects = load_projects()
    if len(argv) > 1 and argv[1] == "-a":
        last_time = None
        print("Buscando os ficheiros coa tradución completa…", end="", flush=True)
    else:
        last_time = int(os.path.getctime('time-token'))
        last_time_string = str(datetime.fromtimestamp(last_time))
        print(
            (
                f"Buscando os ficheiros coa tradución completa e cambios "
                f"desde {last_time_string}…"
            ),
            end="",
            flush=True,
        )
    passed, checked = True, False
    checkers = (
        check_rules,
        check_spelling,
        check_grammar,
    )
    project_paths = _get_project_paths(projects, newer_than=last_time)
    if not project_paths:
        print()
        print("Non se atopou ningún ficheiro con cambios recentes para comprobar!")
        print("Usa -a para comprobalos todos.")
        return
    print(f" Atopáronse {sum(len(item[1]) for item in project_paths)}.")
    for checker in checkers:
        print(f"Executando {checker.__name__}…")
        passed = True
        for project, paths in project_paths:
            passed &= checker(project, paths)
        if not passed:
            return
    print("Pasáronse todas as comprobacións!")


if __name__ == "__main__":
    main()
