import os
from pathlib import Path
from subprocess import run

import yaml

ROOT_FOLDER = Path(__file__).resolve(strict=True).parent.parent


def get_po_folder(project):
    po_folder = ROOT_FOLDER / project["id"]
    if "po_folder" in project:
        po_folder /= project["po_folder"]
    return po_folder


def _get_incomplete_file_paths(project_folder):
    paths = set()
    result = run(
        ["posieve", "stats", "-s", "incompfile:incomplete.txt"],
        cwd=project_folder,
        capture_output=True,
    )
    incomplete_file_path = project_folder / "incomplete.txt"
    with open(incomplete_file_path) as incomplete_file:
        for line in incomplete_file:
            path = line.rstrip()
            paths.add(path)
    os.unlink(project_folder / "incomplete.txt")
    return paths


def get_project_paths(project):
    paths = set()
    project_folder = get_po_folder(project)
    incomplete_file_paths = _get_incomplete_file_paths(project_folder)
    for file_path in project_folder.glob("**/*.po"):
        file_path = str(file_path.relative_to(project_folder))
        if file_path not in incomplete_file_paths:
            paths.add(file_path)
    return paths


def load_projects():
    with (ROOT_FOLDER / "data.yaml").open() as input:
        data = yaml.safe_load(input)
    return [
        {"id": project_id, **project_data}
        for project_id, project_data in data["projects"].items()
    ]
