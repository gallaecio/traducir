from subprocess import call

from utils import ROOT_FOLDER, load_projects


def push(project):
    project_folder = ROOT_FOLDER / project["id"]
    script = str(project_folder / "push.sh")
    call(script, cwd=project_folder)


def main():
    projects = load_projects()
    for project in projects:
        push(project)


if __name__ == "__main__":
    main()
