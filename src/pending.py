import os
import re
from collections import defaultdict
from subprocess import run

from dbus import SessionBus
from polib import pofile

from utils import get_po_folder, get_project_paths, load_projects


class NextTranslations:
    MAX_PATHS = 10

    def __init__(self):
        self._path_count = 0
        # pending word count: set of paths
        self._paths = defaultdict(set)
        self._max_pending_word_count = 0

    def add(self, path, pending_word_count):
        if (
            # Leave documentation for last.
            re.search("appdata|docmessages|documentation|websites", path)
            # Leave untranslated software for later, focus first on existing
            # translations that are out of date.
            or re.search("\.pot$", path)
        ):
            return
        if self._path_count < self.MAX_PATHS:
            self._paths[pending_word_count].add(path)
            self._path_count += 1
            self._max_pending_word_count = max(
                self._max_pending_word_count,
                pending_word_count,
            )
            return
        if pending_word_count > self._max_pending_word_count:
            return
        if pending_word_count == self._max_pending_word_count:
            self._paths[pending_word_count].add(path)
            self._path_count += 1
            return
        assert pending_word_count < self._max_pending_word_count
        self._paths[pending_word_count].add(path)
        self._path_count += 1
        worse_path_count = len(self._paths[self._max_pending_word_count])
        if worse_path_count <= self._path_count - self.MAX_PATHS:
            self._path_count -= worse_path_count
            del self._paths[self._max_pending_word_count]
            self._max_pending_word_count = max(self._paths)

    def load(self):
        bus = SessionBus()
        lokalize_bus_name = next(iter(name for name in bus.list_names() if name.startswith('org.kde.lokalize')))
        lokalize_bus = bus.get_object(lokalize_bus_name, '/ThisIsWhatYouWant')
        open_in_lokalize = lokalize_bus.get_dbus_method('openFileInEditor', 'org.kde.Lokalize.MainWindow')
        lines = [
            "Incomplete translations with the lowest incomplete word counts:",
        ]
        for count in sorted(self._paths):
            lines.append(f"- {count} pending words:")
            for path in sorted(self._paths[count]):
                lines.append(f"  {path}")
                if path.endswith(".pot"):
                    path = path.replace("/templates/", "/gl/")
                    path = path[:-1]
                open_in_lokalize(os.path.abspath(path))
        print("\n".join(lines))


def report(projects):
    next_translations = NextTranslations()
    for project in projects:
        extra_args = []
        parallel_template_replacement = project.get("parallel_template_replacement", None)
        if parallel_template_replacement is not None:
            extra_args.extend(["-s", f"templates:gl:{parallel_template_replacement}"])
        result = run(
            ["posieve", "stats", "-s", "byfile", "-s", "incomplete", *extra_args, get_po_folder(project)],
            capture_output=True,
        )
        in_incomplete = False
        for line in result.stdout.splitlines():
            if not in_incomplete:
                if line.startswith(b"catalog  "):
                    in_incomplete = True
                continue
            line = line.decode()
            pattern = r"(\S+).*?(\d+)$"
            match = re.search(pattern, line)
            path, pending_word_count = match[1], match[2]
            next_translations.add(path, int(pending_word_count))
    next_translations.load()


def main():
    projects = load_projects()
    report(projects)


if __name__ == "__main__":
    main()
