#!/usr/bin/env bash

trap "exit" INT TERM ERR
trap "kill 0" EXIT

nohup languagetool --http &> /dev/null &
ltpid=$!
ps -p $ltpid &> /dev/null || { echo "Could not start LanguageTool, is it installed?" && exit; }

. src/venv/bin/activate
python src/check.py "$@"

kill $ltpid
