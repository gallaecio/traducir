============================
Guións persoais de tradución
============================

Este repositorio contén os guións que uso a nivel persoal para manter e
progresar nas miñas traducións de KDE e Habitica.

O meu fluxo de traballo é:

#.  ``./pull.sh``

    Actualiza as copias locais das traducións.

    Usa o cliente de Subversion para KDE e o cliente de Transifex para
    Habitica. Rexistra a hora da descarga.

    Ao rematar, executa ``./pending.sh``, para aforrarme tempo.

#.  ``./pending.sh``

    Revisa todos os ficheiros de tradución locais incompletos e os modelos
    (ficheiros orixinais aínda sen tradución), os agrupa segundo o número de
    palabras que faltan por traducir, e lista os primeiros grupos ata amosar
    polo menos 10 ficheiros. Se o Lokalize está aberto, abre os ficheiros nel.

#.  Traducir o que se abriu en Lokalize. E, mentres haxa tempo e ganas, volver
    ao paso anterior.

#.  ``./check.sh`` (probablemente o máis interesante para outra xente)

    Atopa os ficheiros modificados desde o último ``./pull.sh`` e coa tradución
    completa, e usa Pology_ para revisalos. Uso unha rama persoal de Pology, a
    `gl <https://invent.kde.org/sdk/pology/-/commits/gl/>`__, que inclúe algúns
    cambios `pendentes de integrar
    <https://invent.kde.org/sdk/pology/-/merge_requests>`__.

    Concretamente, o guión pásalles 3 filtros (sieves), e abre as mensaxes con
    problemas en Lokalize ademais de amosalas no terminal:

    .. _Pology: http://pology.nedohodnik.net/

    #.  As regras de Pology que me interesan.

        Para información detallada sobre como executar regras de Pology e ver
        as regras existentes, véxase
        https://invent.kde.org/sdk/pology/-/tree/master/lang/gl/rules?ref_type=heads

        Non todas as regras valen para todo o mundo, e ultimamente introducín
        moitas regras que pode que só me interesen a min, así que recomendo non
        intentar aplicar todas as regras, senón ir adoptando regras unha a
        unha.

        Para ignorar unha regra ou regras nunha mensaxe concreta, engado nas
        notas de tradución ``skip-rule: <regra1>, <regra2>, …``.

    #.  O corrector ortográfico.

        Teño instalada a rama principal de Git de hunspell-gl_, e se se queixa
        de palabras correctas engádoas a hunspell-gl e reinstalo o corrector
        cos cambios.

        .. _hunspell-gl: https://gitlab.com/trasno/hunspell-gl/

    #.  O corrector gramatical e de estilo.

        Teño instalado o LanguageTool, que o guión inicia automaticamente.

        Teño moitas regras desactivadas por falsos positivos pendentes de
        revisar, e aínda non teño moito xeito coas regras de LanguageTool, así
        que levará tempo.

        Pero de cando en vez detecta algún problema de concordancia de xénero,
        así que creo que paga a pena, a pesar de que é o filtro que máis tempo
        require, e por moito.

    Non se pasa ao filtro seguinte ata que o anterior non atopa problemas.

#.  ``./push.sh``

    Sube os ficheiros cambiados desde o último ``./pull.sh`` (ou
    ``./push.sh``).

A maiores teño un guión, ``./find.sh``, que máis que un guión é un libro de
receitas. Ten código reutilizable para usar Pology para buscar mensaxes que
incumpran regras, buscar por expresións regulares en distintos campos, ignorar
mensaxes especiais (p. ex. listas de palabras clave, onde normalizar non é
axeitado senón que se busca ter sinónimos ou mesmo palabras incorrectas de uso
común), e facer substitucións automáticas.

Cando me apetece máis traballar en mellorar a calidade das traducións
existentes que traducir novas mensaxes, collo ``./find.sh`` para buscar
incumprimentos de regras, por exemplo.
