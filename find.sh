targets="habitica/po/ kde/trunk/l10n-support/gl/summit/"
search="posieve find-messages $targets"
replace="$search -s case"

rule() {
    posieve --skip-obsolete check-rules -s lokalize -s env:habitica habitica/po/ -s rulerx:"$1"
    posieve --skip-obsolete check-rules -s lokalize -s env:kde kde/trunk/l10n-support/gl/summit/ -s rulerx:"$1"
}

# validGroup keywords
# -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"

# Ordes

exit  # Cola

# Historial

# $replace -s msgstr:"\b([Nn]omes?\s+de\s+usuari)o\b" -s replace:"\1a"
# $replace -s msgstr:"\b([Aa]xentes?\s+de\s+usuari)o\b" -s replace:"\1a"
# $replace -s msgid:"\bwallpapers?\b" -s nmsgid:"\bdesktops?\b" -s msgstr:"\b([Ff]ondos?)\s+de\s+(escritorio|pantalla)\b" -s replace:"\1"
# $replace -s msgstr:"\bAmbiente(s?\s+(loca(l|is)|globa(l|is)))\b" -s replace:"Contorno\1"
# $replace -s msgstr:"\bambiente(s?\s+(loca(l|is)|globa(l|is)))\b" -s replace:"contorno\1"

# $replace -s msgstr:"\bOcorreu(\s+un\s+erro\b)" -s replace:"Produciuse\1"
# $replace -s msgstr:"\bocorreu(\s+un\s+erro\b)" -s replace:"produciuse\1"

# $replace -s msgstr:"\baspa(s?)\b" -s replace:"comiña\1"
# $replace -s msgstr:"\bAspa(s?)\b" -s replace:"Comiña\1"

# posieve find-messages kde/trunk/l10n-support/gl/summit/messages/kile/kile.po -s case -s msgstr:"\bAmbiente(s?)\b" -s replace:"Contorno\1"
# posieve find-messages kde/trunk/l10n-support/gl/summit/messages/kile/kile.po -s case -s msgstr:"\bambiente(s?)\b" -s replace:"contorno\1"

# $replace -s msgstr:"\bCopia(s?) de segur(anza|idade)\b" -s replace:"Salvagarda\1" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\bcopia(s?) de segur(anza|idade)\b" -s replace:"salvagarda\1" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
#
# $replace -s msgid:"(?i)\bdaemon\b" -s msgstr:"\bDemo(s?)\b" -s replace:"Servizo\1" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgid:"(?i)\bdaemon\b" -s msgstr:"\bdemo(s?)\b" -s replace:"servizo\1" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
#
# $replace -s msgstr:"\bBandexa(s?) do sistema\b" -s replace:"Área\1 de notificación" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\bbandexa(s?) do sistema\b" -s replace:"área\1 de notificación" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
#
# $replace -s msgstr:"\b([Cc]orreo) non desexado\b" -s replace:"\1 lixo" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"

# $replace -s msgstr:"\bcaseque\b" -s replace:"case"

# $replace -s msgstr:"\b([Uu])tilicen\b" -s replace:"\1sen" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tiliza\b" -s replace:"\1sa" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilizábel\b" -s replace:"\1sábel" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilizal([ao]s?)\b" -s replace:"\1sal\2" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilizar\b" -s replace:"\1sar" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilizando\b" -s replace:"\1sando" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilizad([ao]s?)\b" -s replace:"\1sad\2" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilizou\b" -s replace:"\1sou" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilizarse\b" -s replace:"\1sarse" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilizares\b" -s replace:"\1sares" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\bUtilízanse\b" -s replace:"Úsanse" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\butilízanse\b" -s replace:"úsanse" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\b([Uu])tilice\b" -s replace:"\1se" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\bUtilízase\b" -s replace:"Úsase" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"
# $replace -s msgstr:"\butilízase\b" -s replace:"úsase" -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b"

# $replace -s msgstr:"^Estase a a(\S*[aei])r\b" -s replace:"A\1ndo"
# $replace -s msgstr:"^Estase a b(\S*[aei])r\b" -s replace:"B\1ndo"
# $replace -s msgstr:"^Estase a c(\S*[aei])r\b" -s replace:"C\1ndo"
# $replace -s msgstr:"^Estase a d(\S*[aei])r\b" -s replace:"D\1ndo"
# $replace -s msgstr:"^Estase a e(\S*[aei])r\b" -s replace:"E\1ndo"
# $replace -s msgstr:"^Estase a f(\S*[aei])r\b" -s replace:"F\1ndo"
# $replace -s msgstr:"^Estase a g(\S*[aei])r\b" -s replace:"G\1ndo"
# $replace -s msgstr:"^Estase a h(\S*[aei])r\b" -s replace:"H\1ndo"
# $replace -s msgstr:"^Estase a i(\S*[aei])r\b" -s replace:"I\1ndo"
# $replace -s msgstr:"^Estase a j(\S*[aei])r\b" -s replace:"J\1ndo"
# $replace -s msgstr:"^Estase a k(\S*[aei])r\b" -s replace:"K\1ndo"
# $replace -s msgstr:"^Estase a l(\S*[aei])r\b" -s replace:"L\1ndo"
# $replace -s msgstr:"^Estase a m(\S*[aei])r\b" -s replace:"M\1ndo"
# $replace -s msgstr:"^Estase a n(\S*[aei])r\b" -s replace:"N\1ndo"
# $replace -s msgstr:"^Estase a ñ(\S*[aei])r\b" -s replace:"Ñ\1ndo"
# $replace -s msgstr:"^Estase a o(\S*[aei])r\b" -s replace:"O\1ndo"
# $replace -s msgstr:"^Estase a p(\S*[aei])r\b" -s replace:"P\1ndo"
# $replace -s msgstr:"^Estase a q(\S*[aei])r\b" -s replace:"Q\1ndo"
# $replace -s msgstr:"^Estase a r(\S*[aei])r\b" -s replace:"R\1ndo"
# $replace -s msgstr:"^Estase a s(\S*[aei])r\b" -s replace:"S\1ndo"
# $replace -s msgstr:"^Estase a t(\S*[aei])r\b" -s replace:"T\1ndo"
# $replace -s msgstr:"^Estase a u(\S*[aei])r\b" -s replace:"U\1ndo"
# $replace -s msgstr:"^Estase a v(\S*[aei])r\b" -s replace:"V\1ndo"
# $replace -s msgstr:"^Estase a w(\S*[aei])r\b" -s replace:"W\1ndo"
# $replace -s msgstr:"^Estase a x(\S*[aei])r\b" -s replace:"X\1ndo"
# $replace -s msgstr:"^Estase a y(\S*[aei])r\b" -s replace:"Y\1ndo"
# $replace -s msgstr:"^Estase a z(\S*[aei])r\b" -s replace:"Z\1ndo"
# rule proposta-gl-estase-a-infinitivo

# $replace -s msgstr:"^([Nn]on) se puido (\S*[aei]r)\b" -s replace:"\1 foi posíbel \2"
# $replace -s msgstr:"^([Nn]on) se pode (\S*[aei]r)\b" -s replace:"\1 é posíbel \2"
# $replace -s msgstr:"^([Nn]on) se puideron (\S*[aei]r)\b" -s replace:"\1 foi posíbel \2"
# $replace -s msgstr:"^([Nn]on) se poden (\S*[aei]r)\b" -s replace:"\1 é posíbel \2"
# $replace -s msgstr:"^(Non) puido (\S*[aei]r)se\b" -s replace:"\1 foi posíbel \2"
# $replace -s msgstr:"^(Non) pode (\S*[aei]r)se\b" -s replace:"\1 é posíbel \2"
# $replace -s msgstr:"^(Non) puideron (\S*[aei]r)se\b" -s replace:"\1 foi posíbel \2"
# $replace -s msgstr:"^(Non) poden (\S*[aei]r)se\b" -s replace:"\1 é posíbel \2"

# $replace -s msgid:"^Could\s*not\b" -s msgstr:"^Non se puido (\S*[aei]r)\b" -s replace:"Non foi posíbel \1"
# $replace -s msgid:"^Can\s*not\b" -s msgstr:"^Non se pode (\S*[aei]r)\b" -s replace:"Non é posíbel \1"
# $replace -s msgid:"^Unable to\b" -s msgstr:"^Non se pode (\S*[aei]r)\b" -s replace:"Non é posíbel \1"

# $replace -s msgstr:"\b([Dd]ob)l(emente)\b" -s replace:"\1r\2"
# $replace -s msgstr:"blemente\b" -s replace:"belmente"

# $replace -s msgid:"(?i)\bmenu(-|\s*)bars?\b" -s msgstr:"(\b[Bb]arras?\s*d)o(\s*menú\b)" -s replace:"\1e\2"
# $search -s msgid:"(?i)\bmenu(-|\s*)bars?\b" -s msgstr:"(\b[Bb]arras?\s*de\s*menú)s\b" -s replace:"\1"

# $replace -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b" -s msgstr:"\b([Ss]egur)anza\b" -s replace:"\1idade"

# rule en-artifact
# rule en-alternate
# rule en-address-book

# $replace -s msgid:"(?i)\bprivacy\b" -s msgstr:"\bintimidade\b" -s replace:"privacidade"
# $replace -s msgid:"(?i)\bprivacy\b" -s msgstr:"\bIntimidade\b" -s replace:"Privacidade"

# $replace -s msgstr:"\b([Aa]mig)able\b" -s replace:"\1ábel"
# $replace -s msgstr:"\b([Aa]mig)ables\b" -s replace:"\1ábeis"

# $replace -s msgid:"(?i)\bchat(t(ed|ing)|s)?\b" -s msgstr:"\b([Cc])onversa(s?)\b" -s replace:"\1harla\2"

# $replace -s nmsgctxt:"\b(X-KDE-)?Keywords\b" -s ncomment:"([^,]+,)*\s*keywords\s*(,|$)|\.rst:1\\b" -s msgstr:"\b([Mm]ensax)e(ría)\b" -s replace:"\1a\2"

# $replace -s msgstr:"\bempreguen\b" -s replace:"usen"
# $replace -s msgstr:"\bEmpreguen\b" -s replace:"Usen"
# $replace -s msgstr:"\bempregarase\b" -s replace:"usarase"
# $replace -s msgstr:"\bEmpregarase\b" -s replace:"Usarase"
# $replace -s msgstr:"\bempregou\b" -s replace:"usou"
# $replace -s msgstr:"\bEmpregou\b" -s replace:"Usou"
# $replace -s msgstr:"\bdirectorios\s+e\s+ficheiros\b" -s replace:"cartafoles e ficheiros"
# $replace -s msgstr:"\bDirectorios\s+e\s+ficheiros\b" -s replace:"Cartafoles e ficheiros"

# $replace -s msgid:"(?i)\bsliders?\b" -s msgstr:"\bdesprazador\b" -s replace:"control esvaradío"
# $replace -s msgid:"(?i)\bsliders?\b" -s msgstr:"\bDesprazador\b" -s replace:"Control esvaradío"
# $replace -s msgid:"(?i)\bsliders?\b" -s msgstr:"\bdesprazadores\b" -s replace:"controis esvaradíos"
# $replace -s msgid:"(?i)\bsliders?\b" -s msgstr:"\bDesprazadores\b" -s replace:"Controis esvaradíos"
# $replace -s msgid:"\b[Ww]orking\s*[Dd]irector(y|ies)\b" -s msgstr:"\bdirectorio\s*de\s*traballo\b" -s replace:"cartafol de traballo"
# $replace -s msgid:"\b[Ww]orking\s*[Dd]irector(y|ies)\b" -s msgstr:"\bDirectorio\s*de\s*traballo\b" -s replace:"Cartafol de traballo"
# $replace -s msgid:"\b[Ww]orking\s*[Dd]irector(y|ies)\b" -s msgstr:"\bdirectorios\s*de\s*traballo\b" -s replace:"cartafoles de traballo"
# $replace -s msgid:"\b[Ww]orking\s*[Dd]irector(y|ies)\b" -s msgstr:"\bDirectorios\s*de\s*traballo\b" -s replace:"Cartafoles de traballo"
# $replace -s msgid:"^Authors$" -s msgstr:"^Autores$" -s replace:"Autoría"

# $replace -s msgid:"\b[Rr]oot\s*[Dd]irector(y|ies)\b" -s msgstr:"\bdirectorio\s*raíz\b" -s replace:"cartafol raíz"
# $replace -s msgid:"\b[Rr]oot\s*[Dd]irector(y|ies)\b" -s msgstr:"\bDirectorio\s*raíz\b" -s replace:"Cartafol raíz"
# $replace -s msgid:"\b[Rr]oot\s*[Dd]irector(y|ies)\b" -s msgstr:"\bdirectorios\s*raíz\b" -s replace:"cartafoles raíz"
# $replace -s msgid:"\b[Rr]oot\s*[Dd]irector(y|ies)\b" -s msgstr:"\bDirectorios\s*raíz\b" -s replace:"Cartafoles raíz"

# $replace -s msgstr:"\bmostral([ao]s?)\b" -s replace:"amosal\1"
# $replace -s msgstr:"\bMostral([ao]s?)\b" -s replace:"Amosal\1"
# $replace -s msgid:"\b[Bb]uild\s*[Dd]irector(y|ies)\b" -s msgstr:"\bdirectorio\s*de\s*construción\b" -s replace:"cartafol de construción"
# $replace -s msgid:"\b[Bb]uild\s*[Dd]irector(y|ies)\b" -s msgstr:"\bDirectorio\s*de\s*construción\b" -s replace:"Cartafol de construción"
# $replace -s msgid:"\b[Bb]uild\s*[Dd]irector(y|ies)\b" -s msgstr:"\bdirectorios\s*de\s*construción\b" -s replace:"cartafoles de construción"
# $replace -s msgid:"\b[Bb]uild\s*[Dd]irector(y|ies)\b" -s msgstr:"\bDirectorios\s*de\s*construción\b" -s replace:"Cartafoles de construción"
# $replace -s msgid:"\b[Dd]ownload\s*[Dd]irector(y|ies)\b" -s msgstr:"\bdirectorio\s*de\s*descarga\b" -s replace:"cartafol de descarga"
# $replace -s msgid:"\b[Dd]ownload\s*[Dd]irector(y|ies)\b" -s msgstr:"\bDirectorio\s*de\s*descarga\b" -s replace:"Cartafol de descarga"
# $replace -s msgid:"\b[Dd]ownload\s*[Dd]irector(y|ies)\b" -s msgstr:"\bdirectorios\s*de\s*descarga\b" -s replace:"cartafoles de descarga"
# $replace -s msgid:"\b[Dd]ownload\s*[Dd]irector(y|ies)\b" -s msgstr:"\bDirectorios\s*de\s*descarga\b" -s replace:"Cartafoles de descarga"

# $replace -s msgstr:"\bcambial([ao]s?)\s*de\s*nome\b" -s replace:"renomeal\1"
# $replace -s msgstr:"\bCambial([ao]s?)\s*de\s*nome\b" -s replace:"Renomeal\1"
# $replace -s msgstr:"\bempregaranse\b" -s replace:"usaranse"
# $replace -s msgstr:"\bEmpregaranse\b" -s replace:"Usaranse"
# $replace -s msgstr:"\b([Ff]icheiro\s*ou\s*)directorio\b" -s replace:"\1cartafol"
# $replace -s msgstr:"\b([Ff]icheiros\s*ou\s*)directorios\b" -s replace:"\1cartafoles"
# $replace -s msgid:"^(Displays|Shows)\b" -s msgstr:"^Mostra\b" -s replace:"Amosa"
# $replace -s msgid:"^This\s*(displays|shows)\b" -s msgstr:"^(Isto\s*)mostra\b" -s replace:"\1amosa"
# $replace -s msgstr:"\b((?:[Dd]e|[DdNn]?[Oo]s?|[Nn]ovos?|[Uu]ns?)\s+)script(s?)\b" -s replace:"\1guión\2"
# $replace -s msgstr:"\bscript(s?\s+d[eao]s?)\b" -s replace:"guión\1"
# $replace -s msgstr:"\bScript(s?\s+d[eao]s?)\b" -s replace:"Guión\1"
# $replace -s msgstr:"\bempregouse\b" -s replace:"usouse"
# $replace -s msgstr:"\bRmpregouse\b" -s replace:"Usouse"
# $replace -s msgstr:"\b((?:[Dd]e|[DdNn]?[Oo]s?|[Nn]ovos?|[Uu]ns?)\s+)proxy\b" -s replace:"\1mandatario"
# $replace -s msgstr:"\b((?:[Dd]e|[DdNn]?[Oo]s?|[Nn]ovos?|[Uu]ns?)\s+)proxys\b" -s replace:"\1mandatarios"
# $replace -s msgstr:"\bcambio(s?)\s*de\s*nome\b" -s replace:"renomeamento\1"
# $replace -s msgstr:"\bCambio(s?)\s*de\s*nome\b" -s replace:"Renomeamento\1"

# $replace -s msgstr:"\bmostraranse\b" -s replace:"amosaranse"
# $replace -s msgstr:"\bMostraranse\b" -s replace:"Amosaranse"
# $replace -s msgstr:"\bcambiar\s*o\s*nome\s*d(un(?:ha)?s)\b" -s replace:"renomear \1"
# $replace -s msgstr:"\bCambiar\s*o\s*nome\s*d(un(?:ha)?s|[ao]s?)\b" -s replace:"Renomear \1"
# $replace -s msgstr:"\bcambiar\s*(?:de\s*nome|o\s*nome\s*de)\b" -s replace:"renomear"
# $replace -s msgstr:"\bCambiar\s*(?:de\s*nome|o\s*nome\s*de)\b" -s replace:"Renomear"
# $replace -s msgid:"(?i)\bsliders?\b" -s msgstr:"\besvarador\b" -s replace:"control esvaradío"
# $replace -s msgid:"(?i)\bsliders?\b" -s msgstr:"\bEsvarador\b" -s replace:"Control esvaradío"
# $replace -s msgid:"(?i)\bsliders?\b" -s msgstr:"\besvaradores\b" -s replace:"controis esvaradíos"
# $replace -s msgid:"(?i)\bsliders?\b" -s msgstr:"\bEsvaradores\b" -s replace:"Controis esvaradíos"
# $replace -s msgid:"(?i)\bsliders?\b" -s nmsgid:"(?i)\bscroll(ed|ing|s)?\b" -s msgstr:"\bcontrol\s*de\s*desprazamento\b" -s replace:"control esvaradío"
# $replace -s msgid:"(?i)\bsliders?\b" -s nmsgid:"(?i)\bscroll(ed|ing|s)?\b" -s msgstr:"\bControl\s*de\s*desprazamento\b" -s replace:"Control esvaradío"
# $replace -s msgid:"(?i)\bsliders?\b" -s nmsgid:"(?i)\bscroll(ed|ing|s)?\b" -s msgstr:"\bcontrois\s*de\s*desprazamento\b" -s replace:"controis esvaradíos"
# $replace -s msgid:"(?i)\bsliders?\b" -s nmsgid:"(?i)\bscroll(ed|ing|s)?\b" -s msgstr:"\bControis\s*de\s*desprazamento\b" -s replace:"Controis esvaradíos"
